#!/bin/zsh

# Inferencium
# Kernel - Update

# Copyright 2023 Jake Winters
# SPDX-License-Identifier: BSD-3-Clause-Clear

# Version: 1.0.3.10


major=$(cat Makefile | grep -m 1 "VERSION = " | sed 's/VERSION = //g')
minor=$(cat Makefile | grep -m 1 "PATCHLEVEL = " | sed 's/PATCHLEVEL = //g')
patch=$(cat Makefile | grep -m 1 "SUBLEVEL = " | sed 's/SUBLEVEL = //g')
localversion=$(cat .config | grep "CONFIG_LOCALVERSION=" | sed 's/CONFIG_LOCALVERSION="//g' | sed 's/"//g')

KVER=$major.$minor.$patch$localversion


doas zsh -c\
		"mount /dev/nvme0n1p1 /boot/ &&\
		make --jobs 4 &&\
		make install &&\
		make modules_install &&\
		dracut --kver=${KVER} --compress=zstd --force -a crypt &&\
		grub-mkconfig -o /boot/grub/grub.cfg &&\
		umount /boot/"
