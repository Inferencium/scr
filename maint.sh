#!/bin/zsh

# Inferencium
# Maintenance

# Copyright 2022-2023 Jake Winters
# SPDX-License-Identifier: BSD-3-Clause-Clear

# Version: 1.0.1.6


set -e

doas zsh -c \
	"emerge --sync;
	sleep 10;
	eix-update;
	emerge --update --newuse --changed-use --with-bdeps=y @world;
	emerge --depclean"
