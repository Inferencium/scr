#!/bin/env zsh

# Inferencium
# IP Address - Enable

# Copyright 2022-2023 Jake Winters
# SPDX-License-Identifier: BSD-3-Clause-Clear

# Version: 0.0.4.4


doas zsh -c\
	"ip link set enp37s0 up &&\
	ip addr add 192.168.1.30/24 dev enp37s0 &&\
	ip route add default via 192.168.1.1"
