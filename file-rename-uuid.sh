#!/bin/zsh

# Inferencium
# File - Rename - UUID

# Copyright 2022-2023 Jake Winters
# SPDX-License-Identifier: BSD-3-Clause-Clear

# Version: 1.0.2.4


# Rename all files in current directory to UUIDs
for FILE in *
do
    if [ -f "$FILE" ];then
		ID=`uuidgen`
		EXTENSION=${FILE#*.}
	mv -v "$FILE" "$ID"."$EXTENSION"
	fi
done
