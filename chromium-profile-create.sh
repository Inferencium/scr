#!/bin/sh

# Inferencium
# Chromium - Profile - Create

# Copyright 2022-2023 Jake Winters
# SPDX-License-Identifier: BSD-3-Clause-Clear

# Version: 1.0.2.6


# Chromium profile name
echo "Enter Chromium profile name (no spaces):"
echo ""
read PRF
echo ""

# Chromium profile friendly name
echo "Enter Chromium profile friendly name:"
echo ""
read FRN

# Create Chromium profile
mkdir $HOME/.config/chromium/${PRF}/
echo "[Desktop Entry]" >\
 $HOME/.local/share/applications/chromium-${PRF}.desktop
echo "Name=Chromium (${FRN})" >>\
 $HOME/.local/share/applications/chromium-${PRF}.desktop
echo "Comment=" >> $HOME/.local/share/applications/chromium-${PRF}.desktop
echo "Exec=chromium --user-data-dir=$HOME/.config/chromium/${PRF}" >>\
 $HOME/.local/share/applications/chromium-${PRF}.desktop
echo "Terminal=false" >>\
 $HOME/.local/share/applications/chromium-${PRF}.desktop
echo "Type=Application" >>\
 $HOME/.local/share/applications/chromium-${PRF}.desktop
echo "Icon=chromium" >> $HOME/.local/share/applications/chromium-${PRF}.desktop
echo "Categories=Network;" >>\
 $HOME/.local/share/applications/chromium-${PRF}.desktop
