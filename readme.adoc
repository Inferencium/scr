= Script

Inferencium script files.


[id=branches]
== <<branches,Branches>>

=== https://src.inferencium.net/Inferencium/scr/src/branch/main/[main]

Script repository root directory files pre-alpha development and alpha testing occurs in this
branch. +
Feature-complete modifications of this branch are merged to beta branch for beta testing.

=== https://src.inferencium.net/Inferencium/scr/src/branch/beta/[beta]

Feature-complete beta testing of merged code from development branches occurs in this branch. +
Merges from development branches to this branch are squashed, and the updated versions of the
individual files are mentioned in the commit messages.

=== https://src.inferencium.net/Inferencium/scr/src/branch/stable/[stable]

Feature-complete and tested versions from beta branch are stored in this branch. +
Merges from beta branch to this branch are squashed, and the updated versions of the individual
files are mentioned in the commit messages. +
This branch contains code used in production.


[id=security]
== <<security,Security>>

All files are checked for security issues; however, it is always the user's responsibility to audit
the code before installing and/or executing it. Inferencium takes no responsibility for any security
issues which may arise due to usage of this repository.


[id=licensing]
== <<licensing,Licensing>>

All content is licensed under
https://src.inferencium.net/Inferencium/scr/src/branch/stable/license/BSD-3-Clause-Clear.txt[BSD 3-Clause Clear]
license.
