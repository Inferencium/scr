#!/bin/zsh

# Inferencium
# efibootmgr - Boot Entry - Create

# Copyright 2022-2023 Jake Winters
# SPDX-License-Identifier: BSD-3-Clause-Clear

# Version: 1.0.3.5


doas efibootmgr\
		--disk /dev/nvme0n1\
		--part 1\
		--create\
		--label "custom"\
		--loader '\EFI\custom\vmlinuz'\
		--unicode\
		'root=UUID=[REDACTED] rd.luks.uuid=[REDACTED] quiet splash initrd=\EFI\custom\initramfs.img'
